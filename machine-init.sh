# Install OhMyZsh
sh -c "$(curl -fsSL https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"

# Copy all the configurations
cp -R dots/.* ~
cp -R secrets/.* ~

# Initialize SSH keys
ssh-add -K ~/.ssh/ai_rsa ~/.ssh/github_rsa ~/.ssh/gitlab_rsa ~/.ssh/id_rsa ~/.ssh/id_ed25519
ssh-add -A

# Import GPG keys
gpg --import ~/.gnupg/pubring.gpg

# Install VSCode extensions
code --install-extension bmewburn.vscode-intelephense-client
code --install-extension breezelin.phpstan
code --install-extension bungcip.better-toml
code --install-extension coolbear.systemd-unit-file
code --install-extension dbaeumer.vscode-eslint
code --install-extension DotJoshJohnson.xml
code --install-extension eamodio.gitlens
code --install-extension EditorConfig.EditorConfig
code --install-extension felixfbecker.php-debug
code --install-extension glen-84.sass-lint
code --install-extension ikappas.phpcs
code --install-extension imperez.smarty
code --install-extension Kasik96.latte
code --install-extension lextudio.restructuredtext
code --install-extension marcostazi.VS-code-vagrantfile
code --install-extension mikestead.dotenv
code --install-extension mrmlnc.vscode-apache
code --install-extension ms-azuretools.vscode-docker
code --install-extension ms-vsliveshare.vsliveshare
code --install-extension ms-python.python
code --install-extension mtxr.sqltools
code --install-extension octref.vetur
code --install-extension onecentlin.laravel-blade
code --install-extension rust-lang.rust
code --install-extension whatwedo.twig
code --install-extension wholroyd.jinja
code --install-extension william-voyek.vscode-nginx
